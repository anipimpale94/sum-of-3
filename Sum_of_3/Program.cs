﻿using System;
using System.Collections.Generic;

namespace Sum_of_3 {
  class Program {

    public List<int> numbers = new List<int>();
    public Dictionary<int, bool> checkSum = new Dictionary<int, bool>();
    public int temp1, temp2 = 0;
    public void AddList(List<int> list) {
      foreach(int number in list) {
        int sum = temp1 + temp2;
        numbers.Add(number);
        if(numbers.Count > 2) {
          sum += number;
          checkSum[sum] = true;
        }
        temp1 = temp2;
        temp2 = number;
      }
    }
    public bool ContainsSum(int number) {
      if (checkSum.ContainsKey(number)) {
        return true;
      } else return false;      
    }
    static void Main(string[] args) {
      Program prob = new Program();
      prob.AddList(new List<int>(new int[] { 2,3,4,5 }));
      prob.AddList(new List<int>(new int[] { 6, 7, 8 }));
      Console.WriteLine(prob.ContainsSum(21));
      Console.ReadLine();
    }
  }
}
